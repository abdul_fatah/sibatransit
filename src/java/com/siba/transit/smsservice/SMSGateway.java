package com.siba.transit.smsservice;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 *
 * @author fatah
 */
public class SMSGateway {
    
    static String gatewayIP = "192.168.234.192"; 
    static int port = 7777; 
    
    public static void sendMessage(String phone, String text) throws MalformedURLException, IOException{
        
    
        java.net.URL url = new URL("http://"+gatewayIP+":"+port+"/sendsms?phone="+phone+"&text="+URLEncoder.encode(text)); 
        
        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection(); 
        
        
        urlConn.connect();
        
        
        String msg = urlConn.getResponseMessage(); 
        System.out.println("msg = "+msg);
        
        
        
    }
    
    public static void main(String[] args) throws IOException {
        sendMessage("03133491905", "Hello World"); 
        
    }

}
