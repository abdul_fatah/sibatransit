package com.siba.transit.smsservice;

import com.siba.transit.hibernate.dao.BusDAO;
import com.siba.transit.hibernate.dao.SensorDataDAO;
import com.siba.transit.hibernate.dao.StopDAO;
import com.siba.transit.hibernate.entity.Bus;
import com.siba.transit.hibernate.entity.Location;
import com.siba.transit.hibernate.entity.Stop;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */

@Controller
public class RequestController {

    @RequestMapping(value="/estimate/{busName}",method = RequestMethod.GET)
    public @ResponseBody String getEstimatedDistance(@PathVariable("busName") String busName){
        
        BusDAO dao = new BusDAO();
        Bus bus = dao.getBusByName(busName); 
        
        SensorDataDAO locationDao = new SensorDataDAO(); 
        Location location = locationDao.getRecentLocation(bus.getBusId()+""); 
        
        
        StopDAO stopDao = new StopDAO(); 
        List<Stop> stops = stopDao.listAllStops(); 
    
        float minDistance = Float.MAX_VALUE; 
        Stop nearestStop = null; 
        
        for(Stop stop : stops){
            float distance = (float) gps2m(stop.getLat(),stop.getLon(),Float.parseFloat(location.getLat()),Float.parseFloat(location.getLng())); 
            if(distance < minDistance){
                minDistance = distance; 
                nearestStop = stop; 
            }
        }
        
        return "This bus is "+minDistance+" away from "+nearestStop.getName()+"!"; 
        
    }
    
    private double gps2m(float lat_a, float lng_a, float lat_b, float lng_b) {
    float pk = (float) (180/3.14169);

    float a1 = lat_a / pk;
    float a2 = lng_a / pk;
    float b1 = lat_b / pk;
    float b2 = lng_b / pk;

    float t1 = (float) (Math.cos(a1)*Math.cos(a2)*Math.cos(b1)*Math.cos(b2));
    float t2 = (float) (Math.cos(a1)*Math.sin(a2)*Math.cos(b1)*Math.sin(b2));
    float t3 = (float) (Math.sin(a1)*Math.sin(b1));
    double tt = Math.acos(t1 + t2 + t3);

    return 6366000*tt;
}
    
}
