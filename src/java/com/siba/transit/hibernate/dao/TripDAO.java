package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Route;
import com.siba.transit.hibernate.entity.Trip;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fatah
 */
public interface TripDAO extends GenericDAO<Trip, Serializable>{

    public List<Trip> getTripsByRouteId(Integer routeId); 
    
}
