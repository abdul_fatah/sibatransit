package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Bus;
import com.siba.transit.hibernate.entity.Route;
import java.io.Serializable;

/**
 *
 * @author fatah
 */
public interface RouteDAO extends GenericDAO<Route, Serializable> {

    public Route getRouteByBusId(Integer busId); 
    
}
