package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Driver;

/**
 *
 * @author fatah
 */
public interface DriverDAO extends GenericDAO<Driver, Integer>{

}
