package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Student;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.io.Serializable;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fatah
 */
public class StudentDAOImpl extends GenericDAOImpl<Student, String> 
                implements StudentDao{

    @Override
    public Student findByCms(String cms) {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        session.beginTransaction();
        Query query = session.createQuery("From Student WHERE cms = :cms"); 
        query.setParameter("cms", cms); 
        
        Student student = (Student) query.uniqueResult(); 
        
        session.getTransaction().commit();
        session.close(); 
        
        return student;
    }

    

}
