package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Stop;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import javax.sql.DataSource;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author fatah
 */
public class StopDAO {
    
    
//    @Autowired
//    @Qualifier("gtfsDataSource")
//    private DataSource dataSource; 
    
    public List<Stop> listAllStops(){
        
        List<Stop> stops = null; 
        Session session = null; 
        Transaction tx = null; 
        
        try{
            session = HibernateUtil.getSessionFactory().openSession(); 
            tx = session.beginTransaction(); 
           
            Criteria criteria = session.createCriteria(Stop.class); 
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); 
            
            stops = criteria.list(); 
            
            tx.commit(); 
        }catch(HibernateException e){
            tx.rollback(); 
            System.err.println("Error while loading stops information from DB");            
            
        }finally{
            if(session != null && session.isOpen()){
                session.close();
            }
            
        }
        return stops;
    }
    
    public void addStop(Stop stop){
        
        Session session = null; 
        Transaction tx = null; 
        
        try{
            session = HibernateUtil.getSessionFactory().openSession(); 
            tx = session.beginTransaction(); 
            session.save(stop); 
            
            tx.commit(); 
        }catch(HibernateException e){
            tx.rollback(); 
            System.err.println("Error while saving Stop: "+e.getMessage());            
            
        }finally{
            if(session != null && session.isOpen()){
                session.close();
            }
            
        }
    }
    
    public static void main(String[] args) {
        StopDAO dao = new StopDAO(); 
        dao.addStop(new Stop("sadfa", 0.0f, 0.0f)); 
        
    }
    
}
