package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Bus;
import com.siba.transit.hibernate.entity.Route;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fatah
 */
public class RouteDAOImpl extends GenericDAOImpl<Route, Serializable> implements RouteDAO{

    @Override
    public Route getRouteByBusId(Integer busId) {
        
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        session.beginTransaction(); 
        
        Query query = session.createQuery("From Route WHERE bus.id = :id"); 
        query.setParameter("id", busId); 
        
        List<Route> result = query.list();
        
        session.getTransaction().commit();
        session.close(); 
        
        if(result != null && !result.isEmpty()){
            return result.get(0); 
        }
        
        return null;
    }

   

}
