package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fatah
 */
public abstract class GenericDAOImpl<T,ID extends Serializable> implements GenericDAO<T,ID>{

    @Override
    public T save(T obj) {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
        
        session.close(); 
        
        return obj; 
    }

    @Override
    public void delete(T obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction(); 
        
        session.delete(obj);
        
        session.getTransaction().commit();; 
        session.close(); 
    }

    @Override
    public T findById(Class clazz, ID id) {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        session.beginTransaction(); 
        
        
        T result = (T) session.load(clazz, id);
        
        
        session.getTransaction().commit();; 
        session.close();
        
        return result; 
    }

    @Override
    public List findAll(Class clazz) {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        
        session.beginTransaction(); 
        
        Query query = session.createQuery("From "+clazz.getName()); 
        List result  = query.list(); 
        
        session.getTransaction().commit();
        
        session.close();
        
        return result; 
    }

    @Override
    public T update(T obj) {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        
        session.beginTransaction(); 
        
        session.saveOrUpdate(obj);
        
        session.getTransaction().commit();
        
        session.close();
        
        return obj;
    }

    

}
