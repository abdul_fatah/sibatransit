package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Route;
import com.siba.transit.hibernate.entity.Trip;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fatah
 */
public class TripDAOImpl extends GenericDAOImpl<Trip, Serializable> implements TripDAO{

    @Override
    public List<Trip> getTripsByRouteId(Integer routeId){
        
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        session.beginTransaction(); 
        
        Query query = session.createQuery("FROM Trip WHERE route.routeId=:routeId"); 
        query.setParameter("routeId", routeId); 
        
        List<Trip> result = query.list(); 
        
        session.getTransaction().commit();
        session.close(); 
        
        return result; 
        
    }

}
