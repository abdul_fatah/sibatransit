package com.siba.transit.hibernate.dao;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author fatah
 */
public interface GenericDAO<T, ID extends Serializable> {
    
    void delete(T obj); 
    T findById(Class clazz,ID id); 
    
    T save(T obj); 
    
    public List findAll(Class clazz);
    
    T update(T obj); 
}