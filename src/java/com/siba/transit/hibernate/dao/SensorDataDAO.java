package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Location;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

/**
 *
 * @author fatah
 */
public class SensorDataDAO {
    
//    @Autowired
//    @Qualifier("locationDataSource")
//    DataSource dataSource;

    public void logSensorData(Location data){
        
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        
        Session session = sessionFactory.openSession(); 
        Transaction tx = session.beginTransaction(); 
        
        session.save(data); 
        
        tx.commit(); 
        session.close(); 
        
    }
    
    public Location getRecentLocation(String busId){
        
        
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        Transaction tx = session.beginTransaction(); 
        
        Query query = session.createSQLQuery("SELECT * FROM location ORDER BY "
                + "id DESC LIMIT 1");
        
//        Query query = session.createQuery("FROM Location WHERE busId = :busId ")
        query.setResultTransformer(Transformers.aliasToBean(Location.class)); 
        
        List<Location> locations = query.list();
        
        
        tx.commit(); 
        session.close(); 
        if(!locations.isEmpty())
        return locations.get(0); 
        else return null;
    }
    

}
