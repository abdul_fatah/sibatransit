package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Bus;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.AliasToBeanResultTransformer;

/**
 *
 * @author fatah
 */
public class BusDAO {
    
    public Bus getBusById(String busId){
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        
        Transaction tx = session.beginTransaction();
        
        Query query = session.createQuery("FROM Bus WHERE busId = :busId"); 
        query.setParameter("busId", busId); 
        
        query.setResultTransformer(new AliasToBeanResultTransformer(Bus.class)); 
        
        Bus result = (Bus) query.uniqueResult(); 
        
        tx.commit();
        session.close(); 
        
        return result; 
    }
    
    public List<Bus> getAllBuses(){
         Session session = HibernateUtil.getSessionFactory().openSession(); 
        List<Bus> result = null; 
        Transaction tx = session.beginTransaction();
        
        Query query = session.createQuery("FROM Bus"); 
        
        result = query.list();
        
        tx.commit();
        session.close(); 
        
        return result; 
    }
    
    public void addBus(Bus bus){
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        session.beginTransaction(); 
        session.save(bus);
        
        session.getTransaction().commit();
        session.close(); 
    }
    
    public Bus getBusByName(String busName) {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        
        Transaction tx = session.beginTransaction();
        
        Query query = session.createQuery("FROM Bus WHERE name = :name"); 
        query.setParameter("name", busName); 
        
        query.setResultTransformer(new AliasToBeanResultTransformer(Bus.class)); 
        
        Bus result = (Bus) query.uniqueResult(); 
        
        tx.commit();
        session.close(); 
        
        return result; 
    }

    
    public boolean deleteBusById(int busId){
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        session.beginTransaction(); 
        Query query = session.createQuery("DELETE FROM Bus WHERE busId = :busId"); 
        query.setParameter("busId", busId); 
        
        int rowsAffected = query.executeUpdate(); 
        
        session.getTransaction().commit();
        session.close(); 
        
        return rowsAffected > 0;
    }
    
    public static void main(String[] args){
        BusDAO busDao = new BusDAO(); 
        
        busDao.getAllBuses().forEach(System.out::println); 
    }

    
}
