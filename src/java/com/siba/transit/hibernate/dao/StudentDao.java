/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siba.transit.hibernate.dao;

import com.siba.transit.hibernate.entity.Student;

/**
 *
 * @author fatah
 */
public interface StudentDao extends GenericDAO<Student, String>{
    
    public Student findByCms(String cms);
   
}
