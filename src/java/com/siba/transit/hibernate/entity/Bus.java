package com.siba.transit.hibernate.entity;
// Generated Feb 8, 2016 3:13:53 AM by Hibernate Tools 4.3.1

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bus generated by hbm2java
 */
@Entity
@Table(name = "bus", catalog = "bus_data"
)
public class Bus implements java.io.Serializable {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    
    private Integer busId;
    
    
    private Driver driver;
    private String name;
    @JsonIgnore
    private Set<Route> routes = new HashSet<Route>(0);
    @JsonIgnore
    private Set<Location> locations = new HashSet<Location>(0);

    public Bus() {
    }

    public Bus(Integer busId, Driver driver, String name) {
        this.busId = busId;
        this.driver = driver;
        this.name = name;
    }

    public Bus(Integer busId, Driver driver, String name, Set<Route> routes, Set<Location> locations) {
        this.busId = busId;
        this.driver = driver;
        this.name = name;
        this.routes = routes;
        this.locations = locations;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bus_id", unique = true, nullable = false, length = 20)
    public Integer getBusId() {
        return this.busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "driver_id", nullable = false)
    public Driver getDriver() {
        return this.driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bus")
    public Set<Route> getRoutes() {
        return this.routes;
    }

    public void setRoutes(Set<Route> routes) {
        this.routes = routes;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "busId")
    public Set<Location> getLocations() {
        return this.locations;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

}
