package com.siba.transit.hibernate.entity;
// Generated Feb 8, 2016 3:13:53 AM by Hibernate Tools 4.3.1

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Trip generated by hbm2java
 */
@Entity
@Table(name = "trip", catalog = "bus_data"
)
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "tripId")
public class Trip implements java.io.Serializable {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer tripId;


    @JsonBackReference
    private Route route;
    private Stop stop;

    private String arrival;
    private String departure;
    private int stopSequence;

    public Trip() {
    }

    public Trip(Route route, Stop stop, String arrival, String departure, int stopSequence) {
        this.route = route;
        this.stop = stop;
        this.arrival = arrival;
        this.departure = departure;
        this.stopSequence = stopSequence;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "trip_id", unique = true, nullable = false)
    public Integer getTripId() {
        return this.tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route_id", nullable = false)
    public Route getRoute() {
        return this.route;
    }

    @JsonProperty("route")
    public void setRoute(Route route) {
        this.route = route;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "stop_id", nullable = false)
    public Stop getStop() {
        return this.stop;
    }

    public void setStop(Stop stop) {
        this.stop = stop;
    }

    
    @Column(name = "arrival", nullable = false, length = 20)
    public String getArrival() {
        return this.arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    
    @Column(name = "departure", nullable = false, length = 20)
    public String getDeparture() {
        return this.departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    @Column(name = "stop_sequence", nullable = false)
    public int getStopSequence() {
        return this.stopSequence;
    }

    public void setStopSequence(int stopSequence) {
        this.stopSequence = stopSequence;
        
        
    }

}
