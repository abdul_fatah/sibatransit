package com.siba.transit.hibernate.entity;
// Generated Feb 8, 2016 3:13:53 AM by Hibernate Tools 4.3.1


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Route generated by hbm2java
 */
@Entity
@Table(name = "route", catalog = "bus_data"
)
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "routeId")
public class Route implements java.io.Serializable {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer routeId;
    
    
    private Bus bus;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Stop stopBySource;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Stop stopByDestination;
    
    private String name;
   
    @JsonManagedReference
    private Set<Trip> trips = new HashSet<Trip>(0);

    public Route() {
    }

    public Route(Bus bus, Stop stopBySource, Stop stopByDestination, String name) {
        this.bus = bus;
        this.stopBySource = stopBySource;
        this.stopByDestination = stopByDestination;
        this.name = name;
    }

    public Route(Bus bus, Stop stopBySource, Stop stopByDestination, String name, Set<Trip> trips) {
        this.bus = bus;
        this.stopBySource = stopBySource;
        this.stopByDestination = stopByDestination;
        this.name = name;
        this.trips = trips;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "route_id", unique = true, nullable = false)
    public Integer getRouteId() {
        return this.routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bus_id", nullable = false)
    public Bus getBus() {
        return this.bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "source", nullable = true)
    public Stop getStopBySource() {
        return this.stopBySource;
    }

    public void setStopBySource(Stop stopBySource) {
        this.stopBySource = stopBySource;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "destination", nullable = true)
    public Stop getStopByDestination() {
        return this.stopByDestination;
    }

    public void setStopByDestination(Stop stopByDestination) {
        this.stopByDestination = stopByDestination;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "route",cascade = CascadeType.ALL)
    public Set<Trip> getTrips() {
        return this.trips;
    }

    public void setTrips(Set<Trip> trips) {
        this.trips = trips;
    }

}
