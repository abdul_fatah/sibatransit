package com.siba.transit.hibernate.entity;
// Generated Feb 8, 2016 3:13:53 AM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Alert generated by hbm2java
 */
@Entity
@Table(name="alert"
    ,catalog="bus_data"
    , uniqueConstraints = @UniqueConstraint(columnNames="student_id") 
)
public class Alert  implements java.io.Serializable {


     private Integer id;
     private Calendar calendar;
     private Stop stop;
     private Student student;
     private String alertType;

    public Alert() {
    }

    public Alert(Calendar calendar, Stop stop, Student student, String alertType) {
       this.calendar = calendar;
       this.stop = stop;
       this.student = student;
       this.alertType = alertType;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="calendar_id", nullable=false)
    public Calendar getCalendar() {
        return this.calendar;
    }
    
    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="stop_id", nullable=false)
    public Stop getStop() {
        return this.stop;
    }
    
    public void setStop(Stop stop) {
        this.stop = stop;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="student_id", unique=true, nullable=false)
    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }

    
    @Column(name="alert_type", nullable=false, length=9)
    public String getAlertType() {
        return this.alertType;
    }
    
    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }




}


