package com.siba.transit.hibernate.entity;
// Generated Feb 8, 2016 3:13:53 AM by Hibernate Tools 4.3.1


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sun.org.glassfish.gmbal.IncludeSubclass;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Student generated by hbm2java
 */
@Entity
@Table(name="student"
    ,catalog="bus_data"
)
public class Student  implements java.io.Serializable {


    @JsonInclude(JsonInclude.Include.NON_NULL)
     private String cms;
     private String name;
     private String contact;
     private String email;
     @JsonIgnore
     private Set<Alert> alerts = new HashSet<Alert>(0);

    public Student() {
    }

	
    public Student(String cms, String name, String contact, String email) {
        this.cms = cms;
        this.name = name;
        this.contact = contact;
        this.email = email;
    }
    public Student(String cms, String name, String contact, String email, Set<Alert> alerts) {
       this.cms = cms;
       this.name = name;
       this.contact = contact;
       this.email = email;
       this.alerts = alerts;
    }
   
     @Id 
    @Column(name="cms", unique=true, nullable=false)
    public String getCms() {
        return this.cms;
    }
    
    public void setCms(String cms) {
        this.cms = cms;
    }

    
    @Column(name="name", nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="contact", nullable=false)
    public String getContact() {
        return this.contact;
    }
    
    public void setContact(String contact) {
        this.contact = contact;
    }

    
    @Column(name="email", nullable=false)
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="student")
    public Set<Alert> getAlerts() {
        return this.alerts;
    }
    
    public void setAlerts(Set<Alert> alerts) {
        this.alerts = alerts;
    }




}


