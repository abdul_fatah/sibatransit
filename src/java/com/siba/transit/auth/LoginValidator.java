package com.siba.transit.auth;

/**
 *
 * @author fatah
 */
public interface LoginValidator {

    public boolean validate(String cms, String pwd);
    
}
