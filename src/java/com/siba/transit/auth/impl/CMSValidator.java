package com.siba.transit.auth.impl;

import com.siba.transit.auth.LoginValidator;
import com.siba.transit.util.CMSAuthUtil;

/**
 *
 * @author fatah
 */
public class CMSValidator implements LoginValidator{

    public CMSValidator() {
    }

    
    @Override
    public boolean validate(String cms, String pwd) {
        return CMSAuthUtil.validateUser(cms, pwd);
    }

}
