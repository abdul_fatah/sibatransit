package com.siba.transit.model;

/**
 *
 * @author fatah
 */
public class LocationData {
    
    private String busId; 
    private float lat; 
    private float lon; 

    public LocationData() {
    }

    public String getBusId() {
        return busId;
    }

    public void setBusId(String busId) {
        this.busId = busId;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
    
    

}
