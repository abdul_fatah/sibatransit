package com.siba.transit.model;

/**
 *
 * @author fatah
 */
public class LoginCredentials {

    private String cmsId; 
    private String pwd; 

    public LoginCredentials() {
    }

    public LoginCredentials(String cmsId, String pwd) {
        this.cmsId = cmsId;
        this.pwd = pwd;
    }

    public String getCmsId() {
        return cmsId;
    }

    public void setCmsId(String cmsId) {
        this.cmsId = cmsId;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
    
    
}
