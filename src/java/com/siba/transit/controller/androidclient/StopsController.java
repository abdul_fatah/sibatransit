package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.StopDAO;
import com.siba.transit.hibernate.entity.Stop;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fatah
 */
@Controller
public class StopsController {

    
    @RequestMapping(value = "/stops",produces = MediaType.APPLICATION_JSON_VALUE)
    
    public @ResponseBody List<Stop> getStops(){
        
        StopDAO dao = new StopDAO(); 
        List<Stop> stops = dao.listAllStops(); 
        
        return stops;
    }
    
    @RequestMapping(value = "/stops", method = RequestMethod.POST
            ,consumes = MediaType.APPLICATION_JSON_VALUE
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    
    public ResponseEntity<Stop> addStop(@RequestBody Stop stop){
//        
        StopDAO dao = new StopDAO(); 
        dao.addStop(stop);
        
        return new ResponseEntity<>(stop,HttpStatus.OK); 
    }
}
