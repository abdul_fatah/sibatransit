package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.BusDAO;
import com.siba.transit.hibernate.dao.RouteDAO;
import com.siba.transit.hibernate.dao.RouteDAOImpl;
import com.siba.transit.hibernate.entity.Bus;
import com.siba.transit.hibernate.entity.Route;
import com.siba.transit.hibernate.entity.Trip;
import com.siba.transit.smsservice.SMSGateway;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author fatah
 */

@Controller
public class SMSServiceController {

    @RequestMapping(value = "/receive", method = RequestMethod.GET)
    public void receiveSms(@RequestParam(value = "phone")String phone, 
            @RequestParam("smscenter") String smsCenter, 
            @RequestParam("text") String text){
        
        
        
        String[] tokens = text.split(" "); 
        String command = tokens[0]; 
        String busName = tokens[1]; 
        
        RouteDAO dao = new RouteDAOImpl();
        
        BusDAO busDao = new BusDAO(); 
        Bus bus = busDao.getBusByName(busName); 
        
        StringBuffer message = new StringBuffer();
        if (bus != null) {

            Route route = dao.getRouteByBusId(bus.getBusId());

            Set<Trip> trips = route.getTrips();

            
            for (Trip trip : trips) {
                message.append(trip.getStop().getName() + "," + trip.getArrival() + "," + trip.getDeparture());

            }

        }else{
            message.append("Sorry we could not identify the bus you requested!"); 
        }
        
        try { 
            SMSGateway.sendMessage(phone, message.toString());
        } catch (IOException ex) {
            Logger.getLogger(SMSServiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
