package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.DriverDAO;
import com.siba.transit.hibernate.dao.DriverDAOImpl;
import com.siba.transit.hibernate.entity.Driver;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */

@Controller
public class DriverController {
    
    @RequestMapping(value = "/drivers", method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public ResponseEntity<Driver> addDriver(@RequestBody Driver driver){
    
        DriverDAO dao = new DriverDAOImpl(); 
        dao.save(driver);
        return new ResponseEntity<>(driver,HttpStatus.OK);  
        
    }
    
    @RequestMapping(value = "/drivers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Driver> listDrivers(){
        DriverDAO driverDao = new DriverDAOImpl(); 
        return driverDao.findAll(Driver.class); 
    }

}
