package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.BusDAO;
import com.siba.transit.hibernate.entity.Bus;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */

@Controller

public class BusController {

    @RequestMapping(value = "/buses",method = RequestMethod.POST,
            
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Bus addBus(@RequestBody Bus bus){
        BusDAO dao = new BusDAO(); 
        dao.addBus(bus);
        return bus;
    }
    
    @RequestMapping(value="/buses",method = RequestMethod.GET, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Bus> listBuses(){
        
        BusDAO dao = new BusDAO(); 
        return dao.getAllBuses(); 
    }
    
    @RequestMapping(value="/buses/delete/{id}",method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Boolean deleteBus(@PathVariable("id") int  busId){
        
        BusDAO dao = new BusDAO(); 
        
        boolean result = dao.deleteBusById(busId);
        
        return Boolean.valueOf(result); 
    }
}
