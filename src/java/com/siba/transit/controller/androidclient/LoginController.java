package com.siba.transit.controller.androidclient;

import com.siba.transit.auth.LoginValidator;
import com.siba.transit.util.CMSAuthUtil;
import com.siba.transit.model.LoginCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fatah
 */

@RestController

public class LoginController {

    @Autowired
    @Qualifier("userValidator")
    LoginValidator validator; 
    
    @RequestMapping(value="/login/",method = RequestMethod.POST)
    public ResponseEntity<Boolean> validateCredentials(
            @RequestBody LoginCredentials credentials){
        
        boolean validated =validator.validate(credentials.getCmsId(), credentials.getPwd()); 
        
        return new ResponseEntity<Boolean>(Boolean.valueOf(validated)
                , validated ? HttpStatus.ACCEPTED : HttpStatus.FORBIDDEN); 
        
    }
    
    
}
