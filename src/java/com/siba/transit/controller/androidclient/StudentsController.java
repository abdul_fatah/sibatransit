package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.StudentDAOImpl;
import com.siba.transit.hibernate.entity.Student;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */

@Controller
public class StudentsController {

    @RequestMapping(value = "/students", 
            method = RequestMethod.GET, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Student> listStudents(){
        
        StudentDAOImpl dao = new StudentDAOImpl(); 
        List<Student> students= dao.findAll(Student.class); 
        
        return students; 
    }
    
    @RequestMapping(value = "/studens",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Student addStudent(@RequestBody Student student){
        
        StudentDAOImpl dao = new StudentDAOImpl(); 
        dao.save(student);
        return student; 
    }
}
