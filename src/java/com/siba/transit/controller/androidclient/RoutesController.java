package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.RouteDAO;
import com.siba.transit.hibernate.dao.RouteDAOImpl;
import com.siba.transit.hibernate.entity.Route;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */

@Controller
public class RoutesController {

    
    @RequestMapping(value = "/routes",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Route> getAllRoutes(){

        RouteDAO dao = new RouteDAOImpl(); 
        List<Route> routes = dao.findAll(Route.class); 
        
        return routes; 
        
        
    }
    
    @RequestMapping(value = "/routes", method = RequestMethod.POST, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Route saveRoute(@RequestBody Route route){
        
        RouteDAO dao = new RouteDAOImpl(); 
        dao.update(route);
        
        
        return route; 
    }
    
    @RequestMapping(value = "/routes",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Route updateRoute(@RequestBody Route route){
        
        
        RouteDAO dao = new RouteDAOImpl(); 
        dao.update(route);         
        
        return route; 
        
    }
    
    @RequestMapping(value = "/routes/bus/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Route getRouteByBus(@PathVariable("id") Integer busId) {
        
        
        
        RouteDAO dao = new RouteDAOImpl();
        Route route = dao.getRouteByBusId(busId);

        return route;
    }

    
    
}
