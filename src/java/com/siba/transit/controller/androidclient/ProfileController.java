package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.StudentDAOImpl;
import com.siba.transit.hibernate.dao.StudentDao;
import com.siba.transit.hibernate.entity.Alert;
import com.siba.transit.hibernate.entity.Student;
import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */

@Controller
@RequestMapping("/profile")
public class ProfileController {
    
    
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<Student> getAllProfiles(){
        StudentDao dao = new StudentDAOImpl(); 
        List<Student> students= dao.findAll(Student.class); 
        
        return students; 
        
    }
    
    @RequestMapping(value = "/{cms}",method = RequestMethod.GET)
    public @ResponseBody Student getStudentByCMS(@PathVariable("cms") String cms){
        StudentDao dao =new StudentDAOImpl(); 
        Student student = dao.findByCms(cms); 
        
        
        return student;
        
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody Student saveStudent(@RequestBody Student student){
        StudentDao dao = new StudentDAOImpl(); 
        student = dao.save(student);
        
        return student; 
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody Student updateStudent(@RequestBody Student student){
        StudentDao dao = new StudentDAOImpl(); 
        student = dao.update(student); 
        
        return student; 
    }

}
