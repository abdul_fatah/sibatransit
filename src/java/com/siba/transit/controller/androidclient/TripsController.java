package com.siba.transit.controller.androidclient;

import com.siba.transit.hibernate.dao.TripDAO;
import com.siba.transit.hibernate.dao.TripDAOImpl;
import com.siba.transit.hibernate.entity.Trip;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fatah
 */
@Controller
public class TripsController {

    @RequestMapping(value = "/trips",method = RequestMethod.POST
            ,consumes = MediaType.APPLICATION_JSON_VALUE
            ,produces = MediaType.APPLICATION_JSON_VALUE )
    public @ResponseBody Trip saveTrip(@RequestBody Trip trip){
        
        TripDAO dao = new TripDAOImpl(); 
        dao.save(trip);
        
        return trip;
        
    }
    
    
    @RequestMapping(value = "/trips",method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Trip> getTrips(){
        TripDAO dao = new TripDAOImpl(); 
        List<Trip> trips = dao.findAll(Trip.class); 
        return trips; 
    }
    
    @RequestMapping(value = "/trips/{routeId}"
            ,method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Trip> getTripsByRoute(
            @PathVariable(value = "routeId") Integer routeId){
        
        TripDAO dao = new TripDAOImpl(); 
        List<Trip> trips = dao.getTripsByRouteId(routeId); 
        
        return trips;
    }
}
