package com.siba.transit.controller.logger;

import com.siba.transit.hibernate.dao.BusDAO;
import com.siba.transit.hibernate.dao.SensorDataDAO;
import com.siba.transit.hibernate.entity.Bus;
import com.siba.transit.hibernate.entity.Location;
import com.siba.transit.hibernate.util.HibernateUtil;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.QueryHint;
import javax.websocket.server.PathParam;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author fatah
 */
@RestController

public class LocationLoggerController {

    private final HashMap<String,Bus> busIdMap = new HashMap<>(); 
    
    
    @PostConstruct
    public void retrieveBusInfoFromDb(){
    
        
        
        
    }
    
    @RequestMapping(value = "/loglocation", method = RequestMethod.GET)
    public Boolean logLocation(@RequestParam("id") Integer id, @RequestParam("lat") String lat,
            @RequestParam("lon") String lon) {

        
        SensorDataDAO dao = new SensorDataDAO();
        
        dao.logSensorData(new Location(1, lat, lon));

        return Boolean.TRUE;

    }

    
//    @RequestMapping(value="/loglocation",method=RequestMethod.POST)
    
    @RequestMapping(value = "/location/{busId}", method = RequestMethod.GET)
    public Location getLocation(@PathParam("busId") String busId) {

        SensorDataDAO dao = new SensorDataDAO();
        Location location = dao.getRecentLocation(busId);

        return location;
    }
    
    @RequestMapping(value = "/buses",method = RequestMethod.GET)
    public List<Bus> getAllBuses(){
        BusDAO dao = new BusDAO(); 
        return dao.getAllBuses(); 
    }

}
