package com.siba.transit.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**
 * Component for authenticating CMS credentials of the users
 * Makes a POST request to the CMS server sending the user credentials as form params
 * with the POST request. If the credentials are valid the server sends a Set-Cookie
 * response header. This cookie is attached as a request header to the next request
 * to the "default" page of the CMS user interface, if the server identifies the user
 * then it proceeds to the default page otherwise it again redirects to the login page
 *
 * @author fatah
 *
 */
public class CMSAuthUtil {

    /***REQUEST HEADERS***/
    private static final String PROP_USER_AGENT = "User-Agent";
    private static final String PROP_CONTENT_TYPE = "Content-Type";
    private static final String PROP_ACCEPT_LANGUAGE = "Accept-Language";
    private static final String PROP_ACCEPT_ENCODING = "Accept-Encoding";
    private static final String PROP_HOST = "Host";
    private static final String PROP_ACCEPT = "Accept";

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36";
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String ACCEPT_LANGUAGE ="en-GB,en-US;q=0.8,en;q=0.6";
    private static final String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
    private static final String ACCEPT_ENCODING = "gzip, deflate";
    private static final String HOST = "cms.iba-suk.edu.pk:81";

    private static final String LOGIN_URL = "http://cms.iba-suk.edu.pk:81/psp/hrcs9/?cmd=login&languageCd=ENG";
    private static final String DEFAULT_PAGE_URL = "http://cms.iba-suk.edu.pk:81/psp/hrcs9/EMPLOYEE/HRMS/h/?tab=DEFAULT";

    public static boolean validateUser(String cms,String password) {
        //	wrong password - 7878
        //	correct password - 4265

        String formParams = null;
        try {
            formParams = "face&userid="+cms+"&pwd="+ URLEncoder.encode(password, "UTF-8");


        URL loginUrl = new URL(LOGIN_URL);

        CookieHandler.setDefault(new CookieManager());

        HttpURLConnection loginConn = (HttpURLConnection)
                loginUrl.openConnection();

        loginConn.setRequestMethod("POST");
        loginConn.setDoInput(true);
        loginConn.setDoOutput(true);

        loginConn.setRequestProperty(PROP_HOST, HOST);
        loginConn.setRequestProperty("Origin","http://cms.iba-suk.edu.pk:81");
        loginConn.setRequestProperty(PROP_ACCEPT,ACCEPT);
        loginConn.setRequestProperty(PROP_ACCEPT_ENCODING, ACCEPT_ENCODING);
        loginConn.setRequestProperty(PROP_ACCEPT_LANGUAGE, ACCEPT_LANGUAGE);
        loginConn.setRequestProperty(PROP_CONTENT_TYPE, CONTENT_TYPE);
        loginConn.setRequestProperty(PROP_USER_AGENT, USER_AGENT);

        DataOutputStream formOutput = new DataOutputStream(loginConn.getOutputStream());
        formOutput.writeBytes(formParams);
        formOutput.flush();
        formOutput.close();

//				System.out.println("Response Code: "+loginConn.getResponseCode());
        List<String> setCookie = loginConn.getHeaderFields().get("Set-Cookie");


        //send GET request to the page
        HttpURLConnection pageConn = (HttpURLConnection) new URL(DEFAULT_PAGE_URL).openConnection();

        pageConn.setUseCaches(false);
        pageConn.setDoInput(true);
        pageConn.setDoOutput(true);

        pageConn.setUseCaches(false);
        pageConn.setRequestMethod("GET");
        pageConn.setRequestProperty(PROP_ACCEPT,ACCEPT);
        pageConn.setRequestProperty(PROP_ACCEPT_ENCODING, ACCEPT_ENCODING);
        pageConn.setRequestProperty(PROP_ACCEPT_LANGUAGE, ACCEPT_LANGUAGE);
        pageConn.setRequestProperty(PROP_CONTENT_TYPE, CONTENT_TYPE);
        pageConn.setRequestProperty(PROP_USER_AGENT, USER_AGENT);

        for(String s : setCookie){
            String cookie = s.split(";",1)[0];
//					System.out.println("Adding "+cookie+" to GET request");
            pageConn.addRequestProperty("Cookie", cookie);
        }


//				System.out.println("Page GET Request, response code: "+pageConn.getResponseCode());

        String line;

        try(BufferedReader br = new BufferedReader(new InputStreamReader(pageConn.getInputStream()))){

            while((line = br.readLine()) != null){
                if(line.contains("login")){
                    return false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

            return true;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
