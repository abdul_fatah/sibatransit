package com.siba.transit.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author fatah
 */
public class CorsFilter extends OncePerRequestFilter{

    @Override
    protected void doFilterInternal(HttpServletRequest request, 
            HttpServletResponse response, 
            FilterChain fc) throws 
            ServletException, IOException {
        response.addHeader("Access-Control-Allow-Origin", "*"); 
        if(request.getHeader("Access-Control-Request-Method") != null
                && "OPTIONS".equals(request.getMethod())){
            
            //CORS pre-flight request
            response.addHeader("Access-Control-Allow-Methods","GET,POST,PUT,DELETE"); 
            response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Origin,Content-Type,Accept");
            
        }
        
        fc.doFilter(request, response);
        
    }

    
}
